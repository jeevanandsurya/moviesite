// Function to generate the movie list
function generateMovieList(movies) {
    const movieList = document.getElementById("movie-list");

    // Check if there are no movies available
    if (movies.length === 0) {
        const noMoviesMessage = document.createElement("p");
        noMoviesMessage.textContent = "No movies available. Here's a trailer for past movie:";
        movieList.appendChild(noMoviesMessage);

        // Create a sample movie
        const sampleMovieDiv = document.createElement("div");
        sampleMovieDiv.className = "movie";

        const sampleMovieTitle = document.createElement("h3");
        sampleMovieTitle.textContent = "Sample Movie";
        sampleMovieDiv.appendChild(sampleMovieTitle);

        const sampleTheatreInfo = document.createElement("p");
        sampleTheatreInfo.textContent = "Theatre: Sample Theatre - Location: Sample Location";
        sampleMovieDiv.appendChild(sampleTheatreInfo);

        const sampleReleaseDate = document.createElement("p");
        sampleReleaseDate.textContent = "Release Date: January 1, 2023";
        sampleMovieDiv.appendChild(sampleReleaseDate);

        const sampleBookButton = document.createElement("button");
        sampleBookButton.textContent = "Book Now";
        sampleMovieDiv.appendChild(sampleBookButton);

        movieList.appendChild(sampleMovieDiv);
    } else {
        // Proceed with displaying movies by release year
        const years = [...new Set(movies.map((movie) => movie["release date"].substring(0, 4)))];

        years.forEach((year) => {
            const yearHeading = document.createElement("h2");
            yearHeading.textContent = `Movies Released in ${year}`;
            movieList.appendChild(yearHeading);

            const yearMovies = movies.filter((movie) => movie["release date"].startsWith(year));

            yearMovies.forEach((movie) => {
                const movieDiv = document.createElement("div");
                movieDiv.className = "movie";

                const movieTitle = document.createElement("h3");
                movieTitle.textContent = movie["movie name"];
                movieDiv.appendChild(movieTitle);

                const theatreInfo = document.createElement("p");
                theatreInfo.textContent = `Theatre: ${movie["theatre name"]} - Location: ${movie["theatre location"]}`;
                movieDiv.appendChild(theatreInfo);

                const releaseDate = document.createElement("p");
                releaseDate.textContent = `Release Date: ${movie["release date"]}`;
                movieDiv.appendChild(releaseDate);

                const bookButton = document.createElement("button");
                bookButton.textContent = "Book Now";
                movieDiv.appendChild(bookButton);

                movieList.appendChild(movieDiv);
            });
        });
    }
}
