# Movie Ticket Booking System with Notifications

In today's fast-paced world, people often find it convenient to book movie tickets online, avoiding long queues and last-minute disappointments. As a result, there is a need for a user-friendly and efficient movie ticket booking system. While Node.js is the preferred backend technology, any frameworks or libraries you are comfortable with can be used. Similarly, Angular is the preferred choice for the frontend, but you can use any frameworks or libraries that suit your preferences. This system should allow users to browse available movies, select showtimes, and choose seats. All customer, movie, and booking information needs to be stored in a database, whether it's SQL or NoSQL. Additionally, the system should incorporate a notification feature to enhance the user experience. To make it accessible to users, both frontend and backend components need to be deployed.

![](screenshots/BookTickets.png)
![](screenshots/UserLogin.png)
![](screenshots/UserRegistration.png)
