// Function to handle user registration
document.getElementById("registration-form").addEventListener("submit", function (event) {
    event.preventDefault();

    const formData = new FormData(event.target);

    fetch("registration_process.php", {
        method: "POST",
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            // Registration successful, generate and store JWT
            const token = data.token; // Assuming the server sends the JWT in the response
            localStorage.setItem("jwtToken", token);
            alert("Registration successful!");
        } else {
            alert("Registration failed. " + data.message);
        }
    })
    .catch(error => console.error("Error:", error));
});
