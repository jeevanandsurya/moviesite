// Function to handle user login
document.getElementById("login-form").addEventListener("submit", function (event) {
    event.preventDefault();

    const formData = new FormData(event.target);

    fetch("login_process.php", {
        method: "POST",
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            // Login successful, generate and store JWT
            const token = data.token; // Assuming the server sends the JWT in the response
            localStorage.setItem("jwtToken", token);
            alert("Login successful!");

            // Redirect to BookTickets.html
            window.location.href = "BookTickets.html";
        } else {
            alert("Login failed. " + data.message);
        }
    })
    .catch(error => console.error("Error:", error));
});